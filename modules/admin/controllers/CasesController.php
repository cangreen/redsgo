<?php

namespace app\modules\admin\controllers;

use app\models\Images;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use Yii;
use app\models\Cases;
use app\models\CasesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\CaseImageComponent;
use yii\helpers\Url;

/**
 * CasesController implements the CRUD actions for Cases model.
 */
class CasesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'image-delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin_panel']
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new CasesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Images::find()->where(['case_id' => $id])
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider
        ]);
    }


    public function actionCreate()
    {
        $model = new Cases();

        if ($model->load(Yii::$app->request->post()) && $model->save() && $this->filesUpload($model->id)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
          'query' => Images::find()->where(['case_id' => $id])
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save() && $this->filesUpload($model->id)) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionImageDelete($id)
    {
        $image = Images::findOne($id);
        $model = Cases::findOne($image->case_id);
        if($image->delete()) {
            return $this->redirect([Url::to(['/admin/cases/update', 'id' => $model->id])]);
        }
    }


    protected function findModel($id)
    {
        if (($model = Cases::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function filesUpload($id)
    {
      $model = $this->findModel($id);
      if ($model->load(Yii::$app->request->post())) {
        $model->files = UploadedFile::getInstances($model, 'files');
        if ($model->files && $model->validate()) {
          $dir = 'uploads/' . substr(md5(microtime()), mt_rand(0, 30), 2) . '/' . substr(md5(microtime()), mt_rand(0, 30), 2) . '/';
          if(!is_dir($dir)){
            FileHelper::createDirectory($dir);
          }
          foreach ($model->files as $value) {
            $photo = $value->tempName;
            $name = md5($photo) . '.' . $value->extension;
            $img_src = $dir . $name;
            $value->saveAs($img_src);
            CaseImageComponent::imageProcess($img_src);
            $image = new Images();
            $image->src = $img_src;
            $image->case_id = $model->id;
            $image->save();
          }
        }
      }
      return $model->save();
    }
}
