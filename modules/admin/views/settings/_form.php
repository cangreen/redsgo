<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?php echo $model->name === 'cache_control'
      ? $form->field($model, 'value')->dropDownList([0 => 'Нет', 1 => 'Да'])
      : $form->field($model, 'value')->textInput(['maxlength' => true])
    ?>

    <div class="form-group">
      <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

      <?= Html::a('Отменить', Url::to('/admin/settings'), ['class' => 'btn btn-danger']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
