<?php

use yii\helpers\Html;
use yii\grid\GridView;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => '',
    'columns' => [
      'name',
      'alias',
      [
        'attribute' => 'value',
        'label' => 'Значение',
        'content' => function ($model) {
          if ($model->name === 'cache_control' && $model->value == 1) {
            return 'Да';
          } else if ($model->name === 'cache_control' && $model->value == 0) {
            return 'Нет';
          } else {
            return $model->value;
          }
        }
      ],
      [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{update}',
        'header' => 'Действия',
        'buttons' => [
          'update' => function ($url) {
            return Html::a(
              'Редактировать',
              $url,
              [
                'title' => 'Редактировать',
                'class' => 'btn btn-primary'
              ]
            );
          },
          'delete' => function ($url, $model) {
            return Html::a(
              FAS::icon('trash')->size(FAS::SIZE_LARGE),
              ['delete', 'id' => $model->id],
              [
                'class' => '',
                'title' => 'Удалить',
                'data' => [
                  'confirm' => 'Вы действительно хотите удалить Настройку?',
                  'method' => 'post',
                ],
              ]);
          }
        ],
      ],
    ],
  ]); ?>
</div>
