<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageFile')->fileInput()->widget('kartik\file\FileInput', [
        'language' => 'ru',
        'pluginOptions' => [
            'showUpload' => false,
            // 'showRemove' => false,
            'msgPlaceholder'=>'Загрузка файла',
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="fa fa-lg fa-folder-open"></i>',
            'browseLabel' => 'Выбрать...',
            'allowedFileExtensions' => ['jpg']
        ],

    ])->label('');?>

    <div class="form-group">
        <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

        <?= Html::a('Отменить', Url::to('/admin/contacts'), ['class' => 'btn btn-danger']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
