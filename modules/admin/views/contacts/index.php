<?php

use yii\helpers\Html;
use yii\grid\GridView;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-index">

    <p>
        <?= Html::a('Добавить контакт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            [
                'label' => 'Фото',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Url::to("/$data->contact_photo"),[
                        'alt'=>$data->contact_photo,
                        'style' => 'width:75px;'
                    ]);
                },
            ],
            'contact_name',
            'contact_phone',
            'contact_email:email',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'header' => 'Действия',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a(
                            FAS::icon('eye')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Просмотреть']
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            FAS::icon('pencil-alt')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Редактировать']
                        );
                    },
                    'delete' => function($url, $model){
                        return Html::a(
                            FAS::icon('trash')->size(FAS::SIZE_LARGE),
                            ['delete', 'id' => $model->id],
                            [
                                'class' => '',
                                'title' => 'Удалить',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить контакт?',
                                    'method' => 'post',
                                ],
                            ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
