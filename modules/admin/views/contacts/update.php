<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contacts */

$this->title = "Редактирование контакта: $model->contact_name";
$this->params['breadcrumbs'][] = ['label' => 'Контакты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->contact_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="contacts-update">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => 'Сохранить'
    ]) ?>

</div>
