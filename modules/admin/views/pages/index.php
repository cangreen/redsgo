<?php

use yii\helpers\Html;
use yii\grid\GridView;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <p>
        <?= Html::a('Добавить страницу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            'id',
            'name',
            'title',
            'header',
            'text',
            [ 'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'header' => 'Действия',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a(
                            FAS::icon('eye')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Просмотреть']
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            FAS::icon('pencil-alt')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Редактировать']
                        );
                    },
                    'delete' => function($url, $model){
                        return Html::a(
                            FAS::icon('trash')->size(FAS::SIZE_LARGE),
                            ['delete', 'id' => $model->id],
                            [
                                'class' => '',
                                'title' => 'Удалить',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить страницу?',
                                    'method' => 'post',
                                ],
                            ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
