<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'post')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'imageFile')->fileInput()->widget('kartik\file\FileInput', [
      'language' => 'ru',
      'pluginOptions' => [
        'showUpload' => false,
        // 'showRemove' => false,
        'msgPlaceholder'=>'Загрузка файла',
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="fa fa-lg fa-folder-open"></i> ',
        'browseLabel' => 'Выбрать...',
        'allowedFileExtensions' => ['jpg', 'jpeg']
      ],

    ])->label('');?>

        <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

        <?= Html::a('Отменить', Url::to('/admin/team'), ['class' => 'btn btn-danger']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
