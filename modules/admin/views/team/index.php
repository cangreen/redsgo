<?php

use yii\helpers\Html;
use yii\grid\GridView;
use rmrevin\yii\fontawesome\FAS;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Команда';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index">

    <p>
        <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            'id',
            'name',
            [
                'label' => 'Фото',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Url::to("/$data->photo"),[
                        'alt'=>$data->photo,
                        'style' => 'width:75px;'
                    ]);
                },
            ],
            'post',
            'text',
            'sort',

            [ 'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'header' => 'Действия',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a(
                            FAS::icon('eye')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Просмотреть']
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            FAS::icon('pencil-alt')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Редактировать']
                        );
                    },
                    'delete' => function($url, $model){
                        return Html::a(
                            FAS::icon('trash')->size(FAS::SIZE_LARGE),
                            ['delete', 'id' => $model->id],
                            [
                                'class' => '',
                                'title' => 'Удалить',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить сотрудника?',
                                    'method' => 'post',
                                ],
                            ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
