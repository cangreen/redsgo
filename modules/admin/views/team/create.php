<?php

/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = 'Добавить сотрудника';
$this->params['breadcrumbs'][] = ['label' => 'Команда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-create">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => 'Добавить'
    ]) ?>

</div>
