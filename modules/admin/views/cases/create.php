<?php

/* @var $this yii\web\View */
/* @var $model app\models\Cases */

$this->title = 'Добавление кейса';
$this->params['breadcrumbs'][] = ['label' => 'Кейсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cases-create">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => 'Добавить'
    ]) ?>

</div>
