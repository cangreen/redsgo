<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Cases */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Кейсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cases-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'alias',
            'text',
        ],
    ]) ?>
  <div class="case-view-block">
      <?= ListView::widget([
          'dataProvider' => $dataProvider,
          'itemView' => '_image',
          'itemOptions' => [
              'class' => 'case-view-block'
          ],
          'emptyText' => 'Нет загруженных изображений',
          'summary' => ' '

  ]); ?>

  </div>

</div>
