<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Cases */

$this->title = 'Редактирование кейса: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Кейсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="cases-update">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => 'Сохранить'
    ]) ?>

      <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_image',
        'itemOptions' => [
          'class' => 'case-view-block'
        ],
        'emptyText' => 'Нет загруженных изображений',
        'summary' => ' '

      ]); ?>





</div>
