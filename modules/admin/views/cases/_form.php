<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Cases */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cases-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'files[]')->fileInput(['multiple' => true])->widget(FileInput::className(), [
      'language' => 'ru',
      'options' => [
        'multiple' => true,
      ],
      'pluginOptions' => [
        'showUpload' => false,
       // 'showRemove' => false,
        'msgPlaceholder'=>'Загрузка файла(-ов)',
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="fa fa-lg fa-folder-open"></i> ',
        'browseLabel' => 'Выбрать...',
        'allowedFileExtensions' => ['jpg', 'jpeg']
      ],

    ])->label('');?>

    <div class="form-group">
        <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

        <?= Html::a('Отменить', Url::to('/admin/cases'), ['class' => 'btn btn-danger']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
