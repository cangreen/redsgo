<?php
use yii\helpers\Html;
use yii\helpers\Url;
$action = Yii::$app->requestedAction->id;
use rmrevin\yii\fontawesome\FAS;
?>

<div class="case-view">
  <?= Html::img('/' . $model->src, [
      'width' => 200,
      'class' => 'img img-responsive img-thumbnail'
  ]);
  ?>
</div>
<?= $action == 'update' ?  Html::a('Удалить ' . FAS::icon('times-circle'),
  [Url::to(['/admin/cases/image-delete', 'id' => $model->id])], [
    'class' => 'btn btn-xs btn-danger btn-block',
    'data-method' => 'POST',
    'data-confirm' => 'Удалить изображение?'
  ]) : '';
?>


