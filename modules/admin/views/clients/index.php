<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">

  <p>
    <?= Html::a('Добавить клиента', ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'summary' => '',
    'columns' => [
      'id',
      'name',
      'text',
      'reviewer_name',
      'reviewer_post',
      [
        'label' => 'Лого',
        'format' => 'raw',
        'value' => function ($data) {
          return Html::img(Url::to("/$data->logo"), [
            'alt' => 'Логотип',
            'style' => 'width:75px;'
          ]);
        },
      ],
      'sort',

      ['class' => 'yii\grid\ActionColumn',
        'template' => '{view} {update} {delete}',
        'header' => 'Действия',
        'buttons' => [
          'view' => function ($url) {
            return Html::a(
              FAS::icon('eye')->size(FAS::SIZE_LARGE),
              $url,
              ['title' => 'Просмотреть']
            );
          },
          'update' => function ($url) {
            return Html::a(
              FAS::icon('pencil-alt')->size(FAS::SIZE_LARGE),
              $url,
              ['title' => 'Редактировать']
            );
          },
          'delete' => function ($url, $model) {
            return Html::a(
              FAS::icon('trash')->size(FAS::SIZE_LARGE),
              ['delete', 'id' => $model->id],
              [
                'class' => '',
                'title' => 'Удалить',
                'data' => [
                  'confirm' => 'Вы действительно хотите удалить Клиента?',
                  'method' => 'post',
                ],
              ]);
          }
        ],
      ],
    ],
  ]); ?>
</div>
