<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

  <?= $form->field($model, 'reviewer_name')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'reviewer_post')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'logoFile')->fileInput()->widget('kartik\file\FileInput', [
    'language' => 'ru',
    'pluginOptions' => [
      'showUpload' => false,
      // 'showRemove' => false,
      'msgPlaceholder' => 'Загрузка файла',
      'browseClass' => 'btn btn-primary btn-block',
      'browseIcon' => '<i class="fa fa-lg fa-folder-open"></i> ',
      'browseLabel' => 'Выбрать...',
      'allowedFileExtensions' => ['jpg', 'jpeg', 'gif', 'png']
    ],

  ])->label(''); ?>

  <div class="form-group">
    <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

    <?= Html::a('Отменить', Url::to('/admin/clients'), ['class' => 'btn btn-danger']); ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
