<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clients-view">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => 'Удалить клиента??',
        'method' => 'post',
      ],
    ]) ?>
  </p>

  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
      'name',
      'text',
      'reviewer_name',
      'reviewer_post',
      'sort',
      [
        'label' => 'Лого',
        'format' => 'raw',
        'value' => function ($data) {
          return Html::img(Url::to("/$data->logo"), [
            'alt' => 'Лого',
            'style' => 'width:75px;'
          ]);
        },
      ]
    ],
  ]) ?>

</div>
