<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = "Сменить пароль пользователя: $model->username";
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Смена пароля';
?>
<div class="users-password">

    <div class="users-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'password')->passwordInput([
            'maxlength' => true,
            'autofocus' => true,
            'value'=>''
        ])->label('Введите новый пароль') ?>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?= Html::a('Отменить', Url::to('/admin/users'), ['class' => 'btn btn-danger']); ?>

        <?php ActiveForm::end(); ?>

    </div>


</div>
