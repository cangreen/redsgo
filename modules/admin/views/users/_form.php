<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

$actionId = Yii::$app->controller->action->id;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $actionId == 'create' ? $form->field($model, 'password')->passwordInput() : '' ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

    <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

    <?= Html::a('Отменить', Url::to('/admin/users'), ['class' => 'btn btn-danger']); ?>

    <?php ActiveForm::end(); ?>

</div>
