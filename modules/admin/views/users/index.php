<?php

use yii\helpers\Html;
use yii\grid\GridView;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => '',
        'columns' => [
            'id',
            'username',
            'email:email',
            'surname',
            'name',
            'patronymic',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {password}',
                'header' => 'Действия',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a(
                            FAS::icon('eye')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Просмотреть']
                        );
                    },
                    'update' => function ($url) {
                        return Html::a(
                            FAS::icon('pencil-alt')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Редактировать']
                        );
                    },
                    'delete' => function($url, $model){
                        $role = Yii::$app->authManager->getRolesByUser($model->id);
                        return !isset($role['admin']) ? Html::a(
                            FAS::icon('trash')->size(FAS::SIZE_LARGE),
                            ['delete', 'id' => $model->id],
                            [
                                'class' => '',
                                'title' => 'Удалить',
                                'data' => [
                                    'confirm' => 'Вы действительно хотите удалить пользователя?',
                                    'method' => 'post',
                                ],
                            ]) : '';
                    },
                    'password' => function ($url) {
                        return Html::a(
                            FAS::icon('unlock')->size(FAS::SIZE_LARGE),
                            $url,
                            ['title' => 'Изменить пароль']
                        );
                    },
                ],
            ],
        ],
    ]); ?>
</div>
