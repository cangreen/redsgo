<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */

$this->title = "Редактирование элемента портфолио: $model->name";
$this->params['breadcrumbs'][] = ['label' => 'Портфолио', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="portfolio-update">

    <?= $this->render('_form', [
        'model' => $model,
        'action' => 'Сохранить'
    ]) ?>

</div>
