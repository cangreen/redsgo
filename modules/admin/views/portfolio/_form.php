<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
$caseModels = \app\models\Cases::find()->all();
$cases = ArrayHelper::map($caseModels, 'id', 'alias');
$parentModels = \app\models\Portfolio::findAll(['is_nested' => true]);
$parents = ArrayHelper::map($parentModels, 'id', 'id');
?>

<div class="portfolio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'case_id')->dropDownList($cases, ['prompt' => 'не выбран']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'grid_row_val')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grid_column_val')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'grid_template_columns_val')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_nested')->dropDownList([
            '0'=>'No',
            '1' => 'Yes'
        ]) ?>

    <?= $form->field($model, 'parent_id')->dropDownList($parents, ['prompt' => 'не выбран']) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'imageFile')->fileInput()->widget('kartik\file\FileInput', [
        'language' => 'ru',
        'pluginOptions' => [
            'showUpload' => false,
            // 'showRemove' => false,
            'msgPlaceholder'=>'Загрузка файла',
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="fa fa-lg fa-folder-open"></i>',
            'browseLabel' => 'Выбрать...',
            'allowedFileExtensions' => ['jpg']
        ],

    ])->label('');?>

    <div class="form-group">
        <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

        <?= Html::a('Отменить', Url::to('/admin/portfolio'), ['class' => 'btn btn-danger']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
