<?php rmrevin\yii\fontawesome\AssetBundle::register($this);
    $menuItems = [
        ['label' => 'Настройки сайта', 'icon' => 'cog', 'url' => ['/admin/settings']],
        ['label' => 'Настройки SEO', 'icon' => 'search', 'url' => ['/admin/seo-settings']],
        ['label' => 'Страницы', 'icon' => 'newspaper', 'url' => ['/admin/pages']],
        ['label' => 'Команда', 'icon' => 'users', 'url' => ['/admin/team']],
        ['label' => 'Портфолио', 'icon' => 'book', 'url' => ['/admin/portfolio']],
        ['label' => 'Кейсы', 'icon' => 'briefcase', 'url' => ['/admin/cases']],
        ['label' => 'Клиенты', 'icon' => 'credit-card', 'url' => ['/admin/clients']],
        ['label' => 'Контакты', 'icon' => 'envelope', 'url' => ['/admin/contacts']]
    ];
    Yii::$app->user->can('users') ? array_unshift($menuItems, [
            'label' => 'Пользователи',
            'icon' => 'user',
            'url' => ['/admin/users']]) : '';
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?= \app\widgets\AdminMenu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems
            ]
        ) ?>

    </section>

</aside>
