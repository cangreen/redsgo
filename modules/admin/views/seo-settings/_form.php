<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\SeoSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true, 'disabled' => true]) ?>

    <?= $form->field($model, 'value')->textarea(['maxlength' => true]) ?>

    <div class="form-group">
      <?= Html::submitButton($action, ['class' => 'btn btn-success']) ?>

      <?= Html::a('Отменить', Url::to('/admin/seo-settings'), ['class' => 'btn btn-danger']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
