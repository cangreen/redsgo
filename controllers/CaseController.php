<?php

namespace app\controllers;

use app\models\Cases;
use app\models\Images;
use yii\web\NotFoundHttpException;

class CaseController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
      if ($model = Cases::findOne(['id' => $id])) {
      $images = Images::findAll(['case_id' => $model->id]);
      return $this->render('index', [
        'case' => $model,
        'images' => $images
      ]);
      } else {
        throw new NotFoundHttpException('');
      }
    }

}
