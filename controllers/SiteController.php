<?php

namespace app\controllers;

use app\models\Clients;
use app\models\Contacts;
use app\models\Pages;
use Yii;
use yii\data\ActiveDataFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Team;
use app\models\Portfolio;
use app\models\LoginForm;

class SiteController extends Controller
{
  /**
   * {@inheritdoc}
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout'],
        'rules' => [
          [
            'actions' => ['logout'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
        'layout' => 'error'
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex()
  {
    return $this->render('index');
  }

  public function actionTeam()
  {
    $teamMembers = Team::find()->orderBy(['sort' => SORT_ASC])->all();
    $page = Pages::findOne(['name' => 'team']);
    return $this->render('team', [
      'team' => $teamMembers,
      'page' => $page
    ]);
  }

  public function actionPortfolio()
  {
    $dataProvider = new ActiveDataProvider([
      'query' => Portfolio::find()->orderBy (['sort' => SORT_ASC]),
      'pagination' => [
        'totalCount' => 6,
        'defaultPageSize' => 6,
        'forcePageParam' => false,
      ],
    ]);
    $page = Pages::findOne(['name' => 'portfolio']);
    return $this->render('portfolio', [
      'dataProvider' => $dataProvider,
      'page' => $page
    ]);
  }

  public function actionClients()
  {
    $clients = Clients::find()->orderBy(['sort' => SORT_ASC])->all();
    $page = Pages::findOne(['name' => 'clients']);
    return $this->render('clients', [
      'items' => $clients,
      'page' => $page
    ]);
  }

  public function actionContacts()
  {
    $contacts = Contacts::find()->all();
    $page = Pages::findOne(['name' => 'contacts']);
    return $this->render('contacts', [
      'items' => $contacts,
      'page' => $page
    ]);
  }

  /**
   * Login action.
   *
   * @return Response|string
   */
  public function actionLogin()
  {
    /* if (!Yii::$app->user->isGuest) {
         return $this->goHome();
     }*/

    $model = new LoginForm();
    $this->layout = 'main-login';
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->redirect('/admin');
    }

    $model->password = '';

    return $this->render('login', [
      'model' => $model,
    ]);
  }

  /**
   * Logout action.
   *
   * @return Response
   */
  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }


}
