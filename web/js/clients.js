"use strict";

$(document).ready(function () {
  const windowWidth = $(window).width();
  $(window).on('resize', function () {
    if ($(window).width() !== windowWidth) {
      location.reload();
    }
  });

  $('.scroll-pane').jScrollPane({
    contentWidth: '0px'
  });
});

const clients = document.querySelectorAll('.client__profile');
const logos = document.querySelector('.client__logos');
let ids = [];

clients.forEach(client => {
    ids.push(client.dataset.client);
});
clients.forEach(client => {
    if (client.dataset.client === Math.min(...ids).toString()) {
        client.style.display = 'block'
    }
});

logos.addEventListener('click', event => {
    const elem = event.target;
    const target = elem.tagName === 'IMG' ? elem.parentElement : elem;
    console.dir(target);
    if (target.classList.contains('client__logo')) {
        showClientProfile(target);
    }
});

function showClientProfile(elem) {
    const id = elem.dataset.client;
    clients.forEach(client => {
        if (client.dataset.client === id) {
            client.classList.add('animated', 'fadeInUp');
            client.style.display = 'block';
        } else {
            client.style.display = 'none';
        }
    });
}
