"use strict";

const membersContainer = document.querySelector('.team__members');
const membersCollection = document.querySelectorAll('.team__member-bio');

membersContainer.addEventListener('click', event => {
  if (event.target.classList.contains('team__member-img')) {
    closeAllMembers();
    memberOpen(event.target);
  }
});

function closeAllMembers() {
  const openedMembers = membersContainer.querySelectorAll('.team__member-bio_open');
  if (openedMembers.length !== 0) {
    for (let member = 0; member < openedMembers.length; member++) {
      openedMembers[member].classList.remove('team__member-bio_open');
    }
  }
}

function memberOpen(member) {
  const bio = member.nextElementSibling;
  bio.classList.add('team__member-bio_open');

  const closeButton = member.nextElementSibling.querySelector('.team__member-close');
  closeButton.addEventListener('click', event => memberClose(event.target));

  for (let index = 0; index < membersCollection.length; index++) {
    if (index % 2 !== 0 && membersCollection[index].classList.contains('team__member-bio_open')) {
      bio.classList.add('team__member-bio_open-left');
    }
  }

  window.scrollBy({
    top: bio.getBoundingClientRect().top,
    behavior: 'smooth'
  });
}

function memberClose(member) {
  member.parentElement.parentElement.classList.remove('team__member-bio_open');
}