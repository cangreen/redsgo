"use strict";

const openButton = document.querySelector('.header__menu-button');
const closeButton = document.querySelector('.header__menu-button_close');
const menu = document.querySelector('.header__menu');

setActiveMenuLink();
window.addEventListener("orientationchange", function() {
  this.location.reload();
});

if (document.body.clientWidth <= 720) {
  initClickMenuHandlers();
  initSwipeMenuHandlers();
}

function initClickMenuHandlers() {
  openButton.addEventListener('click', () => runToggleMenu(1));
  closeButton.addEventListener('click', () => runToggleMenu(-1));
}

function initSwipeMenuHandlers() {
  const mcBody = new Hammer(document.body);
  const mcMenu = new Hammer(menu);
  mcBody.on('swiperight', () => runToggleMenu(1));
  mcMenu.on('swipeleft', () => runToggleMenu(-1));
}

function runToggleMenu(direction) {
  if (direction === 1) {
    menu.classList.remove(`slideOutLeft`);
    menu.classList.add('header__menu_open');
    menu.classList.add('animated', `slideInLeft`, 'faster');
    openButton.classList.remove('header__menu-button_visible');
    closeButton.classList.add('header__menu-button_visible');
    document.body.style.overflowY = 'hidden';
  } else {
    menu.classList.remove(`slideInLeft`);
    menu.classList.add(`slideOutLeft`);
    document.body.style.overflowY = 'auto';
    setTimeout(() => openButton.classList.toggle('header__menu-button_visible'), 300);
  }
}

function setActiveMenuLink() {
  const menuLinks = document.querySelectorAll('.header__menu-link');
  const path = document.location.pathname.substr(1);
  let actualPath = null;
  switch (path) {
    case '':
      actualPath = 'index';
      break;
    case 'case':
      actualPath = 'portfolio';
      break;
    default:
      actualPath = path;
  }

  menuLinks.forEach((elem) => {
    if(elem.dataset.menu === actualPath) {
      elem.classList.add('header__menu-link_active');
    }
  });
}
