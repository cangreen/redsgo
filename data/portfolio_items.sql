-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 27 2018 г., 19:01
-- Версия сервера: 5.7.19-log
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `redsgo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio_items`
--

CREATE TABLE `portfolio_items` (
  `id` int(11) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  `case_id` int(11) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `grid_row_val` varchar(16) DEFAULT NULL,
  `grid_column_val` varchar(16) DEFAULT NULL,
  `grid_template_columns_val` varchar(16) DEFAULT NULL,
  `is_nested` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `sort` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `portfolio_items`
--

INSERT INTO `portfolio_items` (`id`, `image`, `case_id`, `text`, `grid_row_val`, `grid_column_val`, `grid_template_columns_val`, `is_nested`, `parent_id`, `sort`) VALUES
(12, 'portfolio1.jpg', 1, 'Lorem ipsum dolor sit amet, adipiscing elit1', '', '', '', 0, NULL, 1),
(13, 'portfolio2.jpg', NULL, 'Lorem ipsum dolor sit amet, adipiscing elit2', '1/3', '2', '', 0, NULL, 2),
(14, 'portfolio3.jpg', NULL, 'Lorem ipsum dolor sit amet, adipiscing elit3', '', '3/5', '', 0, NULL, 3),
(15, 'portfolio4.jpg', NULL, 'Lorem ipsum dolor sit amet, adipiscing elit4', '', '', '', 0, NULL, 4),
(16, 'portfolio5.jpg', NULL, 'Lorem ipsum dolor sit amet, adipiscing elit5', '', '', '', 0, NULL, 5),
(17, 'portfolio6.jpg', NULL, 'Lorem ipsum dolor sit amet, adipiscing elit6', '', '', '', 0, NULL, NULL),
(19, '', NULL, '', '', '1/4', '1fr 1fr', 1, NULL, 6);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `portfolio_items`
--
ALTER TABLE `portfolio_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patent_id_key` (`parent_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `portfolio_items`
--
ALTER TABLE `portfolio_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `portfolio_items`
--
ALTER TABLE `portfolio_items`
  ADD CONSTRAINT `patent_id_key` FOREIGN KEY (`parent_id`) REFERENCES `portfolio_items` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
