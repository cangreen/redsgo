<?php

use yii\db\Migration;

/**
 * Class m181107_145051_add_cases
 */
class m181107_145051_add_cases extends Migration
{

    public function up()
    {
        $this->insert('cases', [
          'name' => 'Amway',
          'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget mauris est. Vestibulum egestas volutpat ornare. Vivamus malesuada est quis magna vestibulum, ut accumsan tortor interdum. Praesent semper, magna nec vestibulum convallis, odio mi dapibus urna, in accumsan ex lacus vel massa.'
        ]);
    }

    public function down()
    {
        $this->truncateTable('cases');
    }
}
