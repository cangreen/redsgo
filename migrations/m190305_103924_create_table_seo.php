<?php

use yii\db\Migration;

/**
 * Class m190305_103924_create_table_seo
 */
class m190305_103924_create_table_seo extends Migration
{
  public function up()
  {
    $this->createTable('seo_settings', [
      'id' => $this->primaryKey()->notNull(),
      'name' => $this->string(64),
      'description' => $this->string(1024),
      'value' => $this->string(4096)
    ]);
  }

  public function down()
  {
    $this->dropTable('seo_settings');
  }

}
