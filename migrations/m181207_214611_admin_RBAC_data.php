<?php

use yii\db\Migration;

/**
 * Class m181207_214611_admin_RBAC_data
 */
class m181207_214611_admin_RBAC_data extends Migration
{

    public function up()
    {
        $this->insert('auth_item', [
            'name' => 'admin',
            'type' => 1
        ]);

        $this->insert('auth_item', [
            'name' => 'moder',
            'type' => 1
        ]);

        $this->insert('auth_item', [
            'name' => 'users',
            'type' => 2
        ]);

        $this->insert('auth_item', [
            'name' => 'admin_panel',
            'type' => 2
        ]);

        $this->insert('auth_item_child', [
            'parent' => 'admin',
            'child' => 'users'
        ]);

        $this->insert('auth_item_child', [
            'parent' => 'admin',
            'child' => 'admin_panel'
        ]);

        $this->insert('auth_item_child', [
            'parent' => 'moder',
            'child' => 'admin_panel'
        ]);

        $this->insert('auth_assignment', [
            'item_name' => 'admin',
            'user_id' => '1'
        ]);


    }

    public function down()
    {
        $this->truncateTable('auth_item');
        $this->truncateTable('auth_item_child');
        $this->truncateTable('auth_assignment');
    }

}
