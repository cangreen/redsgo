<?php

use yii\db\Migration;

/**
 * Class m190107_100357_alter_table_cases
 */
class m190107_100357_alter_table_cases extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('cases', 'alias', 'varchar(64) NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('cases', 'alias');
    }

}
