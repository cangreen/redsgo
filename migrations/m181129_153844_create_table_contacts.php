<?php

use yii\db\Migration;

/**
 * Class m181129_153844_create_table_contacts
 */
class m181129_153844_create_table_contacts extends Migration
{

    public function up()
    {
      $this->createTable('contacts', [
        'id' => $this->primaryKey(),
        'contact_photo' => $this->string(128),
        'contact_name' => $this->string(64),
        'contact_phone' => $this->string(32),
        'contact_email' => $this->string(128),
      ]);
    }

    public function down()
    {
        $this->delete('contacts');
    }
}
