<?php

use yii\db\Migration;

/**
 * Class m181122_144416_fill_table_settings
 */
class m181122_144416_fill_table_settings extends Migration
{

    public function up()
    {
      $this->insert('settings', [
        'name' => 'site_name',
        'alias' => 'Имя сайта',
        'value' => 'Red`s GO!'
      ]);
      $this->insert('settings', [
        'name' => 'adress',
        'alias' => 'Адрес',
        'value' => 'На деревню дедушке, д. 25'
      ]);
      $this->insert('settings', [
        'name' => 'phone',
        'alias' => 'Телефон',
        'value' => '+7 111 11 11'
      ]);
      $this->insert('settings', [
        'name' => 'email',
        'alias' => 'email',
        'value' => 'go@redsgo.ru'
      ]);
      $this->insert('settings', [
        'name' => 'fb_href',
        'alias' => 'Фейсбук',
        'value' => 'https://www.facebook.com/redsgoevent/'
      ]);
      $this->insert('settings', [
        'name' => 'ig_href',
        'alias' => 'Инстаграм',
        'value' => 'https://www.instagram.com/redsgo/'
      ]);
    }

    public function down()
    {
        $this->truncateTable('settings');
    }

}
