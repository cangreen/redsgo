<?php

use yii\db\Migration;

/**
 * Class m181127_140927_alter_portfolio_items
 */
class m181127_140927_alter_portfolio_items extends Migration
{
    public function up()
    {
      $this->addColumn('portfolio_items', 'grid_row_val', 'string(16) null');
      $this->addColumn('portfolio_items', 'grid_column_val', 'string(16) null');
      $this->addColumn('portfolio_items', 'grid_template_columns_val', 'string(16) null');
      $this->addColumn('portfolio_items', 'is_nested', 'boolean default false');
      $this->addColumn('portfolio_items', 'parent_id', 'int(11)');
      $this->addColumn('portfolio_items', 'sort', 'int(3)');

      $this->alterColumn('portfolio_items', 'image', 'string(64) default null');

      $this->addForeignKey('parent_id_key', 'portfolio_items', 'parent_id', 'portfolio_items', 'id');
      $this->addForeignKey('case_id_key', 'portfolio_items', 'case_id', 'cases', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('parent_id_key', 'portfolio_items');
        $this->dropForeignKey('case_id_key', 'portfolio_items');

        $this->dropColumn('portfolio_items', 'grid_row_val');
        $this->dropColumn('portfolio_items', 'grid_column_val');
        $this->dropColumn('portfolio_items', 'grid_template_columns_val');
        $this->dropColumn('portfolio_items', 'is_nested');
        $this->dropColumn('portfolio_items', 'parent_id');
        $this->dropColumn('portfolio_items', 'sort');
    }

}
