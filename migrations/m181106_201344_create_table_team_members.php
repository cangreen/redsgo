<?php

use yii\db\Migration;

/**
 * Class m181106_201344_create_table_team_members
 */
class m181106_201344_create_table_team_members extends Migration
{
    public function up()
    {
        $this->createTable('team_members', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(64)->notNull(),
            'photo' => $this->string(256),
            'post' => $this->string(64)->notNull(),
            'text' => $this->string(1024)->notNull(),
            'sort' => $this->smallInteger()->defaultValue(0),
        ]);

    }

    public function down()
    {
        $this->dropTable('team_members');
    }
}
