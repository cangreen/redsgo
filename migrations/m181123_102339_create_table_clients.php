<?php

use yii\db\Migration;

/**
 * Class m181123_102339_create_table_clients
 */
class m181123_102339_create_table_clients extends Migration
{

    public function up()
    {
      $this->createTable('clients', [
        'id' => $this->primaryKey(),
        'name' => $this->string(128),
        'text' => $this->string(2056),
        'reviewer_name' => $this->string(128),
        'reviewer_post' => $this->string(64),
        'logo' => $this->string(256)
      ]);
    }

    public function down()
    {
        $this->delete('clients');
    }

}
