<?php

use yii\db\Migration;

/**
 * Class m181107_141734_create_table_cases
 */
class m181107_141734_create_table_cases extends Migration
{

    public function up()
    {
      $this->createTable('cases', [
        'id' => $this->primaryKey(),
        'name' => $this->string(64),
        'text' => $this->string(4096)
      ]);
    }

    public function down()
    {
        $this->dropTable('cases');
    }
}
