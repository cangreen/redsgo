<?php

use yii\db\Migration;

/**
 * Class m181207_204726_create_table_users
 */
class m181207_204726_create_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey()->notNull(),
            'username' => $this->string(50)->notNull(),
            'password' => $this->string(128)->notNull(),
            'email' => $this->string(128)->notNull(),
            'surname' => $this->string(20)->notNull(),
            'name' => $this->string(20)->notNull(),
            'patronymic' => $this->string(20)->notNull()
        ]);

    }

    public function down()
    {
        $this->dropTable('users');
    }

}
