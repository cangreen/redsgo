<?php

use yii\db\Migration;

/**
 * Class m181121_194723_fill_table_pages
 */
class m181121_194723_fill_table_pages extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('pages', [
            'name' => 'team',
            'title' => 'Наша команда',
            'header' => 'Наша команда',
            'text' => 'sometext'
        ]);
        $this->insert('pages', [
            'name' => 'portfolio',
            'title' => 'Наше портфолио',
            'header' => 'Наше портфолио',
            'text' => 'sometext'
        ]);
        $this->insert('pages', [
            'name' => 'clients',
            'title' => 'Наши клиенты',
            'header' => 'Наши клиенты',
            'text' => 'sometext'
        ]);
        $this->insert('pages', [
            'name' => 'contacts',
            'title' => 'Наши контакты',
            'header' => 'Наши контакты',
            'text' => ''
        ]);
    }

    public function down()
    {
        $this->truncateTable('pages');
    }

}
