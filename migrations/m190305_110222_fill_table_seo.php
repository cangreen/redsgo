<?php

use yii\db\Migration;

/**
 * Class m190305_110222_fill_table_seo
 */
class m190305_110222_fill_table_seo extends Migration
{
  /**
   * {@inheritdoc}
   */

  public function up()
  {
    $this->insert('seo_settings', [
      'name' => 'meta_keywords',
      'description' => 'список ключевых слов (key words), соответствующих содержимому страницы сайта. Поисковые системы могут использовать ключевые слова тега meta name keywords content при индексации',
      'value' => 'Red`sGo, event, ивент агентство, 2018, Москва, организация event, btl, тимбилдинг, лучшее эвент агентство, сайт эвент агентства, мероприятие организация, корпоратив организация, новогодний корпоратив, организация, корпоратив под ключ, корпоратив проведение, корпоратив на природе, организация тематического корпоратива, корпоратив на 8 марта, новогодний корпоратив 2019'
    ]);

    $this->insert('seo_settings', [
      'name' => 'meta_description',
      'description' => 'Мета-описание страницы сайта. Содержимое meta name description content используется при формировании сниппета для описания сайта в поиске.',
      'value' => 'Event-агентство Red\'sGo. Прямo сейчас мы делаем: корпоратив, за который не стыдно, идеальную свадьбу, лучший день рождения (ваш), конференции, выставки и презентации.'
    ]);

    $this->insert('seo_settings', [
      'name' => 'og_image',
      'description' => 'URL-адрес изображения Open Graph',
      'value' => 'rg-og.jpg'
    ]);

    $this->insert('seo_settings', [
      'name' => 'og_description',
      'description' => 'Одно-два предложения описания в Open Graph',
      'value' => 'Event-агентство Red\'sGo. Прямo сейчас мы делаем: корпоратив, за который не стыдно, идеальную свадьбу, лучший день рождения (ваш), конференции, выставки и презентации.'
    ]);
  }

  public function down()
  {
    $this->truncateTable('seo_settings');
  }
}
