<?php

use yii\db\Migration;

/**
 * Class m181107_143051_create_table_images
 */
class m181107_143051_create_table_images extends Migration
{
    public function up()
    {
      $this->createTable('images', [
        'id' => $this->primaryKey(),
        'src' => $this->string(256)->notNull(),
        'case_id' => $this->integer(11)->null()
      ]);
    }

    public function down()
    {
        $this->dropTable('images');
    }
}
