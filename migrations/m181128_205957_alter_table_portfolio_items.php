<?php

use yii\db\Migration;

/**
 * Class m181128_205957_alter_table_portfolio_items
 */
class m181128_205957_alter_table_portfolio_items extends Migration
{

    public function up()
    {
        $this->addColumn('portfolio_items', 'name', 'string(64) NOT NULL DEFAULT " " AFTER id');
    }

    public function down()
    {
        $this->dropColumn('portfolio_items', 'name');
    }

}
