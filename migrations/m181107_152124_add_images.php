<?php

use yii\db\Migration;

/**
 * Class m181107_152124_add_images
 */
class m181107_152124_add_images extends Migration
{
    public function up()
    {
      $this->insert('images', [
        'src' => 'case1.png',
        'case_id' => 1
      ]);
      $this->insert('images', [
        'src' => 'case2.png',
        'case_id' => 1
      ]);
      $this->insert('images', [
        'src' => 'case3.png',
        'case_id' => 1
      ]);
    }

    public function down()
    {
        $this->truncateTable('images');
    }
}
