<?php

use yii\db\Migration;

/**
 * Class m181107_133219_create_table_portfolio_items
 */
class m181107_133219_create_table_portfolio_items extends Migration
{

    public function up()
    {
      $this->createTable('portfolio_items', [
        'id' => $this->primaryKey(),
        'image' => $this->string(64)->notNull(),
        'case_id' => $this->integer(11)->null(),
        'text' => $this->string(256)->null()
      ]);
    }

    public function down()
    {
        $this->dropTable('portfolio_items');
    }
}
