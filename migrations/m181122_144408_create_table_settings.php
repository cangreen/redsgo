<?php

use yii\db\Migration;

/**
 * Class m181122_144408_create_table_settings
 */
class m181122_144408_create_table_settings extends Migration
{

    public function up()
    {
      $this->createTable('settings', [
        'id' => $this->primaryKey(),
        'name' => $this->string(128)->unique(),
        'alias' => $this->string(128),
        'value' => $this->string(128)
      ]);
    }

    public function down()
    {
        $this->dropTable('settings');
    }

}
