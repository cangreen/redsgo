<?php

use yii\db\Migration;

/**
 * Class m190213_135220_alter_table_clients_add_sort
 */
class m190213_135220_alter_table_clients_add_sort extends Migration
{

    public function up()
    {
      $this->addColumn('clients', 'sort', 'smallint default 100');
    }

    public function down()
    {
        $this->dropColumn('clients', 'sort');
    }
}
