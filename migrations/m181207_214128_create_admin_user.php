<?php

use yii\db\Migration;

/**
 * Class m181207_214128_create_admin_user
 */
class m181207_214128_create_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */

    public function up()
    {
        $this->insert('users', [
            'id' => 1,
            'username' => 'admin',
            'email' => 'exlatent2@gmail.com',
            'password' => '$2y$13$JjKhjYFq7CIAIr8cG0Otc.tt11Ku.H2GMlVFcizM8RZk0xqiPwLeG',
            'surname' => 'Админ',
            'name' => 'Админ',
            'patronymic' => 'Админ',
        ]);
    }

    public function down()
    {
        $this->truncateTable('users');
    }

}
