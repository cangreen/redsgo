<?php

use yii\db\Migration;

/**
 * Class m181121_194115_create_table_pages
 */
class m181121_194115_create_table_pages extends Migration
{

    public function up()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32)->notNull(),
            'title' => $this->string(128),
            'header' => $this->string(128)->notNull(),
            'text' => $this->string(1024)
        ]);
    }

    public function down()
    {
        $this->delete('pages');
    }

}
