<?php

use yii\db\Migration;

/**
 * Class m181107_133637_add_portfolio_items
 */
class m181107_133637_add_portfolio_items extends Migration
{
    public function up()
    {
        $this->insert('portfolio_items', [
          'image' => 'portfolio1.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit1',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio2.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit2',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio3.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit3',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio4.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit4',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio5.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit5',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio6.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit6',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio7.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit7',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio10.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit8',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio2.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit9',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio10.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit10',
        ]);

        $this->insert('portfolio_items', [
          'image' => 'portfolio5.jpg',
          'case_id' => 1,
          'text' => 'Lorem ipsum dolor sit amet, adipiscing elit11',
        ]);
    }

    public function down()
    {
        $this->truncateTable('portfolio_items');
    }
}
