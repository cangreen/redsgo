<?php
/**
 * Created by PhpStorm.
 * User: cangreen
 * Date: 10.11.18
 * Time: 13:17
 */

namespace app\widgets;


use dmstr\widgets\Menu;

class AdminMenu extends Menu
{
    public static $iconClassPrefix = 'fas fa-';
}