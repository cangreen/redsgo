<?php
/* @var $this yii\web\View */
\app\assets\CaseAsset::register($this);
\rmrevin\yii\fontawesome\AssetBundle::register($this);

use rmrevin\yii\fontawesome\FAS;
$this->title = "Red`sGO! | {$case['name']}";
?>
  <div class="content content__case">
    <div class="container">
      <p class="content__text"><?= $case['text'] ?></p>
      <div class="slick__gallery">
          <?php foreach ($images as $img) : ?>
            <div class="slick__pic md-4 sm-12"><img class="slick__img" src="<?= $img['src'] ?>" alt="Case image">
            </div>
          <?php endforeach; ?>
      </div>
    </div>
  </div>
<?php
$angle_left = FAS::icon('angle-left', ['class' => 'slick__icon slick__icon_left']);
$angle_right = FAS::icon('angle-right', ['class' => 'slick__icon slick__icon_right']);


$this->registerJs("
    if(document.body.clientWidth > 720) {
    $('.slick__gallery').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      prevArrow: '<a href=\"#\" class=\"slick__arrow slick__arrow_left\">$angle_left</a>',
      nextArrow: '<a href=\"#\" class=\"slick__arrow slick__arrow_right\">$angle_right</a>'
    });
  }
", 3);


