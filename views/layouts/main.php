<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\helpers\RedDotAnimationHelper;

AppAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/favicon-96x96.png'])]);
$action = Yii::$app->controller->action->id;
$controller = Yii::$app->controller->id;
$page = \app\models\Pages::findOne(['name' => $action]);
if (!is_null($page)) {
    $head = $page->title;
} else {
  $case = \app\models\Cases::findOne(Yii::$app->request->get('id'));
  $head = $case['name'];
}
\rmrevin\yii\fontawesome\AssetBundle::register($this);

use rmrevin\yii\fontawesome\FAB;
function isPageHasScroll($action, $controller){
  return ($action !== 'index' || $controller === 'case');
}


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php if (Yii::$app->settings->cache_control == 0): ?>
      <meta http-equiv="Cache-Control" content="no-cache">
    <?php endif; ?>
  <meta name="description" content="<?= Yii::$app->seoSettings->meta_description ?>">
  <meta name="keywords" content="<?= Yii::$app->seoSettings->meta_keywords ?>">
  <meta property="og:title" content="<?= $this->title ?>">
  <meta property="og:type" content="website">
  <meta property="og:image" content="<?= Url::home(true); ?>uploads/<?= Yii::$app->seoSettings->og_image ?>">
  <meta property="og:url" content="<?= Yii::$app->request->absoluteUrl; ?>">
  <meta property="og:locale" content="ru-RU">
  <meta property="og:description" content="<?= Yii::$app->seoSettings->og_description ?>">
  <meta name="yandex-verification" content="54019cfa42a959c3"/>
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
      (function (m, e, t, r, i, k, a) {
          m[i] = m[i] || function () {
              (m[i].a = m[i].a || []).push(arguments)
          };
          m[i].l = 1 * new Date();
          k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
      })
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

      ym(52654978, "init", {
          clickmap: true,
          trackLinks: true,
          accurateTrackBounce: true,
          webvisor: true
      });
  </script>
  <noscript>
    <div><img src="https://mc.yandex.ru/watch/52654978" style="position:absolute; left:-9999px;" alt=""/></div>
  </noscript>
  <!-- /Yandex.Metrika counter -->
    <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header class="header header_<?= Yii::$app->controller->id == 'site' ? $action : Yii::$app->controller->id ?>">
  <div class="container container_menu">
    <button class="header__menu-button header__menu-button_visible">
      <span class="header__button-bar"></span>
      <span class="header__button-bar"></span>
      <span class="header__button-bar"></span>
    </button>
    <nav class="header__menu <?= isPageHasScroll($action, $controller) ? 'scrollfixed' : ''; ?>">
      <button class="header__menu-button header__menu-button_close ">
        <span class="header__button-bar header__button-bar_close"></span>
        <span class="header__button-bar header__button-bar_close"></span>
      </button>
      <ul class="header__menu-items">
        <li class="header__menu-item"><?= Html::a('Главная', ['/'], ['class' => 'header__menu-link', 'data-menu' => 'index']) ?></li>
        <li class="header__menu-item"><?= Html::a('Команда', ['/team'], ['class' => 'header__menu-link', 'data-menu' => 'team']) ?></li>
        <li class="header__menu-item"><?= Html::a('Портфолио', ['/portfolio'], ['class' => 'header__menu-link', 'data-menu' => 'portfolio']) ?></li>
        <li class="header__menu-item"><?= Html::a('Клиенты', ['/clients'], ['class' => 'header__menu-link', 'data-menu' => 'clients']) ?></li>
        <li class="header__menu-item"><?= Html::a('Контакты', ['/contacts'], ['class' => 'header__menu-link', 'data-menu' => 'contacts'])?></li>
        <li class="header__menu-item"><div class="header__menu-item header__menu-item_separator"></div></li>
        <li class="header__menu-item header__menu-item_contacts">
          <a href="mailto:<?= Yii::$app->settings->email; ?>"
             class="header__menu-item_contact"><?= Yii::$app->settings->email; ?></a>
        </li>
      </ul>
    </nav>
    <div class="header__decoration-line <?= isPageHasScroll($action, $controller) ? 'scrollfixed' : ''; ?>">
      <div class="header__decoration-dot <?= RedDotAnimationHelper::render(); ?>"></div>
    </div>
  </div>
    <?php if ($controller === 'site' && $action === 'index'): ?>
      <div class="header__social">
        <div class="container">
          <div class="header__social-wrapper">
            <a href="<?= Yii::$app->settings->fb_href; ?>"
               target="_blank"><?= FAB::icon('facebook-f', ['class' => 'fa-2x header__social-icon']) ?></a>
            <a href="<?= Yii::$app->settings->ig_href; ?>"
               target="_blank"> <?= FAB::icon('instagram', ['class' => 'fa-2x header__social-icon']) ?></a>
          </div>
        </div>
      </div>
    <?php else: ?>
      <div class="container header__head_container">
        <div class="header__head_bg"></div>
        <h1 class="header__head_h1"><?= $head ?></h1>
      </div>
    <?php endif; ?>
</header>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
