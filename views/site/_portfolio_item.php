<a class="portfolio__grid-block"
   style="<?php if (!empty($model->grid_row_val)): ?>grid-row: <?= $model->grid_row_val ?> ; <?php endif; ?>
   <?php if (!empty($model->grid_column_val)): ?>grid-column: <?= $model->grid_column_val ?><?php endif; ?>"
   href="/case?id=<?= $model->case_id ?>" data-id="<?= $model->id ?>">
  <div class="portfolio__grid-text">
    <span class="portfolio__grid-span"><?= $model->name ?></span>
  </div>
  <img class="portfolio__grid-img" src="<?= $model->image ?>" alt="<?= $model->name ?>">
</a>