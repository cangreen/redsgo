<?php

use yii\widgets\ListView;

$this->title = Yii::$app->settings->site_name . ": $page->title";
?>

    <div class="content content__portfolio">
        <div class="container">
            <p class="content__text">
                <?= $page->text ?>
            </p>
            <?php
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_portfolio_item',
                'itemOptions' => [
                    'tag' => false,
                    'class' => 'portfolio__grid-block'
                ],
                'pager' => [
                    'class' => \kop\y2sp\ScrollPager::className(),
                    'item' => '.portfolio__grid-block',
                    'container' => '.portfolio__grid',
                    'paginationSelector' => '.pagination',
                    'noneLeftText' => '',
                    'triggerText' => '',
                ],
                'summary' => '',
                'options' => [
                    'class' => 'portfolio__grid'
                ],

            ]);
            ?>
        </div>
    </div>
<?php
$this->registerJs('
$(window).scroll(()=>{
if($(window).scrollTop()+$(window).height()>=$(document).height()){
    $(\'.ias-trigger\').trigger(\'click\');
}
});
$(window).on({
\'touchmove\': ()=>{
if($(window).scrollTop()+$(window).height()>=$(document).height()){
    $(\'.ias-trigger\').trigger(\'click\');
  }
}
});



if(window.location.search) {
  document.location.href = "/portfolio";
}
');
?>