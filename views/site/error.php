<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$code = Yii::$app->response->getStatusCode();
$this->title = Yii::$app->settings->site_name . ": $code";
const ERR_404 = 'Страница, которую вы ищете, не существует. Она может быть полностью перемещена или удалена.';
const ERR_404_NAME = 'Эта страница не найдена';
?>
<main class="page404">
    <section class="page404-bg"></section>
    <section class="page404-body">
        <h1 class="page404-h1"><?= $code == '404' ? ERR_404_NAME : $name ?></h1>
        <div class="page404-minus"></div>
        <p class="page404-text"><?= $code == '404' ? ERR_404 : $name ?></p>
        <?=Html::a('на главную', '/', ['class' => 'btn page404-home']);?>
        <?php
        print_r($message);
        ?>
    </section>
</main>
