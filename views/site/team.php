<?php
\app\assets\TeamAsset::register($this);

$this->title = Yii::$app->settings->site_name . ": $page->title";
?>
<div class="content content__team">
  <div class="container">
    <p class="content__text">
      <?= $page->text ?>
    </p>
    <div class="row team__members">
      <?php if (isset($team)): ?>
      <?php foreach ($team as $member): ?>
      <div class="team__member lg-3 sm-12">
        <div class="team__member-img" style="background-image: url('<?= $member['photo']; ?>')"></div>
        <div class="team__member-bio">
          <div class="team__member-img_inner" style="background-image: url('<?= $member['photo']; ?>')"></div>
          <div class="team__member-body">
            <div class="team__member-close"></div>
            <h2 class="team__member-header"><?= $member['name']; ?></h2>
            <span class="team__member-post"><?= $member['post']; ?></span>
            <p class="team__member-text">
              <?= $member['text']; ?>
            </p>
          </div>
        </div>
      </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
</div>