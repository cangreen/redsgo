<?php
\app\assets\ClientsAsset::register($this);
$this->title = Yii::$app->settings->site_name . ": $page->title";
?>

<div class="content content__clients">
  <div class="container container_fluid">
    <div class="row row_inner">
      <div class="lg-6">
        <div class="client__logos scroll-pane">
          <div class="row row_inner">
              <?php if (isset($items)): ?>
                  <?php foreach ($items as $item): ?>
                  <div class="client__logo lg-4 md-6" data-client="<?= $item->id ?>">
                    <img class="client__img" src="<?= $item->logo ?>" alt="<?= $item->name ?>">
                  </div>
                  <?php endforeach; ?>
              <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="lg-6 sm-12 client__profiles">
          <?php if (isset($items)): ?>
              <?php foreach ($items as $item): ?>
              <div class="client__profile" data-client="<?= $item->id ?>">
                <h2 class="client__header"><?= $item->name ?></h2>
                <div class="client__logo_mobile" style="background-image: url('<?= $item->logo ?>')">
                </div>
                <p class="client__text">
                    <?= $item->text ?>
                </p>
              </div>
              <?php endforeach; ?>
          <?php endif; ?>
      </div>
    </div>
  </div>
</div>