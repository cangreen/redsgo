<?php
\rmrevin\yii\fontawesome\AssetBundle::register($this);

use rmrevin\yii\fontawesome\FAB;

$settings = Yii::$app->settings;
$this->title = $settings->site_name . ": $page->title";
?>
<div class="content content__contacts">
  <div class="container container_fluid">
    <div class="row">
        <?php foreach ($items as $contact) : ?>
          <div class="contact__wrapper lg-3 sm-12">
            <div class="contact__image" style="background-image: url('<?= $contact['contact_photo'] ?>')"></div>
            <p class="contact__name"><span class="contact__span"><?= $contact['contact_name'] ?></span></p>
            <p class="contact__phone-mail">
              <a class="contact__phone"
                 href="tel:<?= $contact['contact_phone'] ?>"><?= $contact['contact_phone'] ?></a> <br>
              <a class="contact__mail"
                 href="mailto:<?= $contact['contact_email'] ?>"> <?= $contact['contact_email'] ?></a>
            </p>
          </div>
        <?php endforeach; ?>
      <div class="contact__wrapper contact__wrapper_flex lg-3 sm-12">
        <div class="contacts__adress">
          <p class="contacts__adress-text">Адрес: <?= $settings->adress ?></p>
          <a class="contact__mail contact__mail_common" href="mailto:<?= $settings->email ?>"><?= $settings->email ?></a>
        </div>
        <div class="contacts__social">
          <a class="contacts__social-icon" href="<?= $settings->fb_href ?>" target="_blank"><?= FAB::icon('facebook-f') ?></a>
          <a class="contacts__social-icon" href="<?= $settings->ig_href ?>" target="_blank"><?= FAB::icon('instagram') ?></a>
        </div>
      </div>
    </div>
  </div>
</div>