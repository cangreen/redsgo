<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email', 'surname', 'name', 'patronymic'], 'required'],
            [['username'], 'string', 'max' => 50],
            [['password', 'email'], 'string', 'max' => 128],
            [['surname', 'name', 'patronymic'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
            'email' => 'Email',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'patronymic' => 'Отчество',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $user = Users::findOne(['id' => $this->id]);
        if ($insert) {
            $user->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $user->save();
            $am = Yii::$app->authManager;
            $role = $am->getRole('moder');
            $am->assign($role, $this->id);
        }
    }
}
