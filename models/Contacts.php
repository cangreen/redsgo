<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $contact_photo
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $contact_email
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $imageFile;

    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_name'], 'string', 'max' => 64],
            [['contact_photo'], 'string', 'max' => 128],
            [['contact_phone'], 'string', 'max' => 32],
            [['contact_email'], 'string', 'max' => 128],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg'  ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contact_photo' => 'Фото',
            'contact_name' => 'Имя',
            'contact_phone' => 'Телефон',
            'contact_email' => 'Email',
        ];
    }
}
