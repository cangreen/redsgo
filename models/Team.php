<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team_members".
 *
 * @property int $id
 * @property string $name
 * @property string $photo
 * @property string $post
 * @property string $text
 * @property int $sort
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_members';
    }

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'post', 'text'], 'required'],
            [['sort'], 'integer'],
            [['name', 'post'], 'string', 'max' => 64],
            [['photo'], 'string', 'max' => 256],
            [['text'], 'string', 'max' => 1024],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'  ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'photo' => 'Фото',
            'post' => 'Должность',
            'text' => 'Текст',
            'sort' => 'Сортировка',
        ];
    }
}
