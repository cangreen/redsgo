<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cases".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $alias
 */
class Cases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cases';
    }

    public $files;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'string', 'max' => 64],
            [['text'], 'string', 'max' => 4096],
            [['files'], 'file', 'maxFiles' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Текст',
            'alias' => 'Код',
        ];
    }
}
