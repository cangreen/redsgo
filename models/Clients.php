<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $reviewer_name
 * @property string $reviewer_post
 * @property string $logo
 */
class Clients extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'clients';
  }

  public $logoFile;

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['name', 'reviewer_name'], 'string', 'max' => 128],
      [['text'], 'string', 'max' => 2056],
      [['reviewer_post'], 'string', 'max' => 64],
      [['logo'], 'string', 'max' => 256],
      [['sort'], 'integer'],
      [['logoFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'name' => 'Бренд',
      'text' => 'Текст',
      'reviewer_name' => 'Имя сотрудника',
      'reviewer_post' => 'Должность сотрудника',
      'logo' => 'Логотип',
      'sort' => 'Сортировка'
    ];
  }
}
