<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Portfolio;

/**
 * PortfolioSearch represents the model behind the search form of `app\models\Portfolio`.
 */
class PortfolioSearch extends Portfolio
{
    /**
     * {@inheritdoc}
     */
    public $caseName;

    public function rules()
    {
        return [
            [['id', 'case_id', 'is_nested', 'parent_id', 'sort'], 'integer'],
            [['image', 'name', 'text', 'grid_row_val', 'grid_column_val', 'grid_template_columns_val', 'caseName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        if (($pos = strrpos($attribute, '.')) !== false) {
            $modelAttribute = substr($attribute, $pos + 1);
        } else {
            $modelAttribute = $attribute;
        }
        $value = $this->$modelAttribute;
        if (trim($value) === '') {
            return;
        }

        $attribute = "user_roles.$attribute";
        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Portfolio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC]
                ],
                'name' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC]
                ],
                'image' => [
                    'asc' => ['image' => SORT_ASC],
                    'desc' => ['image' => SORT_DESC]
                ],
                'caseName' => [
                    'asc' => ['cases.name' => SORT_ASC],
                    'desc' => ['cases.name' => SORT_DESC]
                ],
                'text' => [
                    'asc' => ['text' => SORT_ASC],
                    'desc' => ['text' => SORT_DESC]
                ],
                'is_nested' => [
                    'asc' => ['is_nested' => SORT_ASC],
                    'desc' => ['is_nested' => SORT_DESC]
                ],
                'parent_id' => [
                    'asc' => ['parent_id' => SORT_ASC],
                    'desc' => ['parent_id' => SORT_DESC]
                ],
                'sort' => [
                    'asc' => ['sort' => SORT_ASC],
                    'desc' => ['sort' => SORT_DESC]
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->joinWith(['cases']);
            return $dataProvider;
        }

        $this->addCondition($query, 'case_id');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'portfolio_items.name' => $this->name,
            'case_id' => $this->case_id,
            'is_nested' => $this->is_nested,
            'parent_id' => $this->parent_id,
            'sort' => $this->sort,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'portfolio_items.name', $this->name])
            ->andFilterWhere(['like', 'portfolio_items.text', $this->text])
            ->andFilterWhere(['like', 'grid_row_val', $this->grid_row_val])
            ->andFilterWhere(['like', 'grid_column_val', $this->grid_column_val])
            ->andFilterWhere(['like', 'grid_template_columns_val', $this->grid_template_columns_val]);

        $query->joinWith(['case' => function ($q) {
            $q->where('IFNULL(cases.name,1)  LIKE "%' . $this->caseName . '%"');
        }]);

        return $dataProvider;
    }
}
