<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "portfolio_items".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property int $case_id
 * @property string $text
 * @property string $grid_row_val
 * @property string $grid_column_val
 * @property string $grid_template_columns_val
 * @property int $is_nested
 * @property int $parent_id
 * @property int $sort
 *
 * @property Cases $case
 * @property Portfolio $parent
 * @property Portfolio[] $portfolios
 */
class Portfolio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $imageFile;

    public static function tableName()
    {
        return 'portfolio_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['case_id', 'is_nested', 'parent_id', 'sort'], 'integer'],
            [['image', 'name'], 'string', 'max' => 64],
            [['text'], 'string', 'max' => 256],
            [['grid_row_val', 'grid_column_val', 'grid_template_columns_val'], 'string', 'max' => 16],
            [['case_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cases::className(), 'targetAttribute' => ['case_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Portfolio::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg'  ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Фото',
            'case_id' => 'Кейс',
            'text' => 'Текст',
            'grid_row_val' => 'grid-row',
            'grid_column_val' => 'grid-column',
            'grid_template_columns_val' => 'grid-template-columns',
            'is_nested' => 'nested',
            'parent_id' => 'Родительский элемент',
            'sort' => 'Порядок',
            'caseName' => 'Кейс'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCase()
    {
        return $this->hasOne(Cases::className(), ['id' => 'case_id']);
    }

    public function getCaseName()
    {
        return $this->case['alias'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Portfolio::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPortfolios()
    {
        return $this->hasMany(Portfolio::className(), ['parent_id' => 'id']);
    }
}
