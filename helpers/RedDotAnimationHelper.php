<?php
namespace app\helpers;

use Yii;


class RedDotAnimationHelper
{
  public static function render()
  {
    $route = self::getRoute();
    if(!self::pageWasRefreshed() && isset($route))
      if($route[0] < $route[1]) {
        return 'animated slideInLeft' . ($route[1] - $route[0]) . ' delay-' . ($route[1] - $route[0]);
      } else if ($route[0] > $route[1]) {
        return 'animated slideInRight' . ($route[0] - $route[1]) . ' delay-' . ($route[0] - $route[1]);
      } else {
          return '';
      }
  }

  private static function getRoute()
  {
    $from_route = Yii::$app->request->referrer;
    $from = isset($from_route) ? explode('/', $from_route)[3] : '';
    $to = mb_substr(Yii::$app->request->url, 1);
    $route = [];
    switch($from){
      case '':
        $route[] = 0;
        break;
      case 'team':
        $route[] = 1;
        break;
      case 'portfolio':
        $route[] = 2;
        break;
      case 'clients':
        $route[] = 3;
        break;
      case 'contacts':
        $route[] = 4;
        break;
      default:
        $route[] = 2;
    }
    switch($to){
      case '':
        $route[] = 0;
        break;
      case 'team':
        $route[] = 1;
        break;
      case 'portfolio':
        $route[] = 2;
        break;
      case 'clients':
        $route[] = 3;
        break;
      case 'contacts':
        $route[] = 4;
        break;
      default:
          $route[] = 2;
    }
    return $route;
  }

  private static function pageWasRefreshed(){
    return isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
  }
}