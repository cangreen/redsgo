<?php
/**
 * Created by PhpStorm.
 * User: cangreen
 * Date: 08.11.18
 * Time: 22:14
 */

namespace app\assets;


use dmstr\web\AdminLteAsset;

class AdminAsset extends AdminLteAsset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/AdminLTE.css',
        'css/skins/_all-skins.css'
    ];

    public $skin = '';


}