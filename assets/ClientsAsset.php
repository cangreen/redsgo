<?php
/**
 * Created by PhpStorm.
 * User: i.borisov
 * Date: 23.11.2018
 * Time: 15:07
 */

namespace app\assets;


class ClientsAsset extends AppAsset
{
  public $js = [
    'js/clients.js?v=101',
    'js/jquery.jscrollpane.min.js',
    'js/jquery.mousewheel.js',
  ];
}