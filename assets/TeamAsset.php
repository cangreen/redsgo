<?php

namespace app\assets;

class TeamAsset extends AppAsset
{
    public $js = [
        'js/team.js',
    ];
}