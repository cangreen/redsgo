<?php
/**
 * Created by PhpStorm.
 * User: i.borisov
 * Date: 07.11.2018
 * Time: 18:42
 */

namespace app\assets;


class CaseAsset extends AppAsset
{
  public $js = [
    'js/slick.min.js',
  ];
}