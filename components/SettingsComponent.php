<?php
/**
 * Created by PhpStorm.
 * User: cangreen
 * Date: 22.11.18
 * Time: 0:49
 */

namespace app\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;

class SettingsComponent extends Component
{
    private $_attributes;

    public function init() {
      parent::init();
      $this->_attributes = ArrayHelper::map(\app\models\Settings::find()->all(), 'name', 'value');
    }

    public function __get($name) {
      if (array_key_exists($name, $this->_attributes))
        return $this->_attributes[$name];

      return parent::__get($name);
  }
}