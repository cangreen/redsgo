<?php

namespace app\components;

use yii\imagine\Image;

class CaseImageComponent extends \yii\base\Component
{

    public static function imageProcess($img_src)
    {
      $img = Image::getImagine()->open($img_src);
      $width = $img->getSize()->getWidth();
      $height = $img->getSize()->getHeight();
      if($width > $height){
        Image::crop($img_src, $height, $height, [($width-$height)/2, 0])->save($img_src);
      }else{
        Image::crop($img_src, $width, $width, [0, ($height - $width)/2])->save($img_src);
      }
      Image::resize($img_src, 400, 400)->save($img_src);
    }
}